## General Information

- [pricing-api](https://gitlab.com/kavak-it/pricing-api)
- [Confluence - pricing-api](https://kavak.atlassian.net/wiki/spaces/PIR/overview?homepageId=2451538238)

## Prerequisite and Software

- Install java version 17.0.5 or higher
- Gradle 7.4.2 will be installed or higher
- Spring boot 2.7.5
- Docker Installation

## CI / CD

This project has CI/CD configured, when the code merges with a common branch you can check the result on **pricing-api.[stage].kavak.io/** (where stage is 'dev', 'qa', etc).
### Argo CD

- [Dev](https://delivery.plt.kavak.io/applications/pricing-api.dev.kavak.io?resource=)
- [Prod](https://delivery.plt.kavak.io/applications/pricing-api.prd.kavak.io?resource=)

## How to use

### Services

###### Initiate docker db and migrations container

```sh
$ docker-compose up db dbmate
```

###### Run application

```sh
$ Localize the main application of 'pricing-api' project in
$ src > main > java > com.kavak.pricing.api > Application.java
```
```sh
$ Right click Application.java file > Run 'Application.main()'
```
```sh
$ Application will initialize in the specfied port (default :8080)
```
###### Verify in the browser that app is up an available
```sh
$ localhost:8080
```
###### or if endpoints are expose, verify in postman with api given path
```sh
$ eg: localhost:8080/api/version
```
### Test

###### All 'pricing-api' tests
```sh
$ Right click on 'pricing-api' project > Run 'Tests in 'pricing-api''
```

###### Individual testing files

```sh
$ Right click on 'test file' > Run 'Tests file'
```

###### Individual test execution

```sh
$ Click the 'Play' button at the left side of the desired 'Test'
```
###### Test metrics

```sh
$ docker-compose up statsd
```

### Build

```sh
$  Right click on pricing-api proyect > Build Module 'pricing-api'
```
### Rebuild

```sh
$  Right click on pricing-api proyect > Rebuild Module 'pricing-api'
```
### Deploy

```sh
$
```
