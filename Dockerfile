ARG KAVAK_PLATFORM_SERVICE=unknown_application

FROM amazoncorretto:17 as builder
WORKDIR /app
ARG ARTIFACTORY_USERNAME
ARG ARTIFACTORY_ACCESS_TOKEN
RUN curl -Lo /usr/bin/dbmate https://github.com/amacneil/dbmate/releases/download/v1.13.0/dbmate-linux-amd64 \
    && chmod +x /usr/bin/dbmate
ARG KAVAK_PLATFORM_SERVICE
COPY . .
ENV GRADLE_USER_HOME=`pwd`/.gradle
ENV GRADLE_OPTS="-Dorg.gradle.daemon=false"
RUN chmod +x ./gradlew
RUN ./gradlew assemble

FROM amazoncorretto:17.0.1-alpine3.14
ARG KAVAK_PLATFORM_SERVICE
ENV KAVAK_PLATFORM_SERVICE=${KAVAK_PLATFORM_SERVICE}
ENV SPRING_PROFILES_ACTIVE=pod
COPY db/migrations db/migrations
COPY .kavak /app/.kavak
COPY dd-lib/dd-java-agent.jar /app/
COPY --from=builder /usr/bin/dbmate /usr/bin/dbmate
COPY --from=builder app/build/libs/*.jar app.jar
ENV LOGBACK_APPENDER=json
ENV LOGBACK_LEVEL=INFO
EXPOSE 8080
CMD ["java", "-javaagent:/app/dd-java-agent.jar", "-jar", "app.jar"]